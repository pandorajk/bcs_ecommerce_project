import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import axios from 'axios';
import './App.css'

//user pages:
import Navbar from './components/Navbar'
import Home from "./components/Home"
import About from "./components/About"
import SignUp from './components/SignUp'
import Login from './components/Login'
import ProductIndividual from "./components/ProductIndividual"
import Products from "./components/Products"
import Member from "./components/Member"

//admin pages:
import Admin from "./components/Admin"
import AdminGenre from "./components/AdminGenre"
import AdminBook from "./components/AdminBook"
import UpdateBook from './components/Updatebook'
import AdminReview from "./components/AdminReview"

//PAYMENT IMPORTS...........
import Basket from "./components/Basket";
import Payment from "./components/Payment";
import OrderConfirmation from "./components/OrderConfirmation";

// ****CONDITONAL URL
import {URL} from './config'

class App extends React.Component {
    
state = {
  //BOOK STATE:
    genres : [], 
    books : [],  
    individualBook : '',
  //LOGIN STATE:
    user : {},
    login : false,
    adminAccess : ''
}

componentDidMount(){
   this.findAllGenres()
   this.findAllBooks()
   this.verify_token()
}

findAllGenres = async () => {
  try{
    const response = await axios.get(`${URL}/genre/findallgenres`)
    this.setState({genres : response.data.response})
  }
  catch (err) {}
}

findGenreByGenreID = async e => {
  if(e.target.value === 'select')return null
  if(e.target.value === 'all')return this.findAllGenres()
  axios.post(`${URL}/genre/findgenrebygenreID`, {_id : e.target.value})
  .then (res => {
      this.setState ({genres : res.data.response})})
  .catch (error => console.log(error))
}

findAllBooks = async () => {
  try{
    const response = await axios.get(`${URL}/book/findallbooks`)
    this.setState({books : response.data.response})
  }   
  catch (err) {}
}

findBooksByGenre = async e => {
  if(e === 'all' || e.target && e.target.value === 'all')return this.findAllBooks()
  axios.post(`${URL}/book/findbooksbygenre`, { genre_id : e.target ? e.target.value : e }) 
  .then (res => {
    this.setState ({books : res.data.response})
  })
  .catch (error => console.log(error))
}

verify_token = async () => {
  try{
      const token = JSON.parse(localStorage.getItem('token'))
      const response = await axios.post(`${URL}/user/verify_token`,{token})
      if (response.data.ok) {
          this.setState({login : true, user : {user_id : response.data.user_id, firstname : response.data.firstname, email : response.data.email}}) 
      }
      if (response.data.adminAccess) {
        this.setState({adminAccess : true})
      }
      return response.data
  }
  catch(error){
    console.log(error)
  }
}

logout = () => {
  localStorage.removeItem('token') 
  this.setState({login : false, user : {}, adminAccess : false })
}

userEmail = (user) => {
    this.setState({user})
}

setStateBooks = (books) => {
  this.setState({books})
}

  render() {
    console.log('App.js State', this.state)
    return (
      <div>
        <div>
        <Router >
          <Navbar logout={this.logout}/>
          <Route exact path="/" render={props =><Home {...props} {...this.state} 
                                                        findBooksByGenre={this.findBooksByGenre} 
                                                        setStateBooks={this.setStateBooks} /> } />
          <Route exact path="/about" render={props =><About /> } />
          <Route path="/signup" render={props =><SignUp {...props} {...this.state}  />} />
          <Route path="/login" render={props =><Login {...props} {...this.state} 
                                                        verify_token={this.verify_token}/>} />

          <Route path="/product/:book_id" render={props =><ProductIndividual {...props} {...this.state} /> } />
          <Route path="/products" render={props =><Products {...props} {...this.state} 
                                                              findAllGenres={this.findAllGenres}
                                                              findBooksByGenre={this.findBooksByGenre}
                                                              findAllBooks={this.findAllBooks} /> } />

          <Route exact path="/member" render={props =><Member {...props} {...this.state} logout={this.logout} userEmail={this.userEmail}/> }/> 

          <Route exact path="/admin" render={props =><Admin {...props} {...this.state} />} />
          <Route exact path="/admin/genre" render={props =><AdminGenre {...props}
                                                                  {...this.state}
                                                                  findGenreByGenreID={this.findGenreByGenreID} 
                                                                  findAllGenres={this.findAllGenres} /> } />
          <Route exact path="/admin/book" render={props =><AdminBook {...props} {...this.state} 
                                                                     findAllGenres={this.findAllGenres}
                                                                     findBooksByGenre={this.findBooksByGenre} /> } />
          <Route exact path="/admin/review" render={props =><AdminReview {...props} {...this.state}/> } />
          
          <Route path="/admin/updatebook" render={props =><UpdateBook {...props} {...this.state} /> } />

          <Route path="/basket" render={props =><Basket {...props} {...this.state} /> } />
          <Route path="/payment" render={props =><Payment {...props} {...this.state} /> } /> 
          <Route path="/order_confirmation" render={props =><OrderConfirmation {...props} {...this.state} /> } /> 
          
        </Router>
        </div>
        <footer>
          The Latin American Bookstore | E-Commerce Project | Built with React, Express and MongoDB | Pandorajk 2019
        </footer>
      </div>
    )
  }
}

export default App;