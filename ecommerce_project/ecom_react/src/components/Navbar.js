import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = (props) => {
  return (
    <div className="nav">
      <div className="nav1a">
        <NavLink to={"/"}>Home</NavLink>
      </div>
      <div className="nav1b">
        <div className="nav1c"><NavLink to={"/signup"}>Sign Up</NavLink></div>
        <div className="nav1d"><NavLink to={"/login"}>Members</NavLink>
            <div className="membersDropdown">
                <p><NavLink to={"/login"}>Login</NavLink></p>
                <p><NavLink to={"/member"}>Account</NavLink></p>
                <p onClick={()=>props.logout()}>Logout</p>
              </div>
          </div>
        <div className="nav1e"><NavLink to={"/basket"}>Basket</NavLink></div>
      </div>
    </div>
  );
};

export default Navbar;