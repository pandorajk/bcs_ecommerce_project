import React from 'react'

class AllbooksAdmin extends React.Component{

  render(){
    return (
        <div>
            {this.props.allbooks.map((ele,i) => {
              let image = ele.image
            return <div key={i} className="box">
                    <div className="admindashboard">
                      <div>
                        <p><img src={require("../images/books/"+image)} className="bookcover" alt="Book cover"></img></p>
                        <p><button onClick={()=> this.props.history.push({pathname : '/admin/updatebook', state: ele })}>Update book</button></p>
                        <p><button onClick={e => {if (window.confirm('Are you sure you wish to delete this book?')) this.props.submitRemove(e, ele._id) } }>Remove BOOK</button></p> 
                      </div>
                      <div className="memberinfo">
                        <h4>Title: {ele.title}</h4>
                        <p>Author: {ele.author}</p>
                        <p>Publishing details: {ele.publishing}</p>
                        <p>ISBN/ASIN: {ele.isbn_asin}</p>
                        <p>Price: {ele.price} €</p>
                        <p>Genre id: {ele.genre_id}</p>
                        <p>Genre id2: {ele.genre_id2}</p>
                        <p>Bestseller: {ele.bestseller === true ? `Yes` : `No`}</p>
                        <p>Stock: {ele.stock}</p>
                        <p>Image URL: {ele.image}</p>
                        <p>Blurb: {ele.blurb}</p>
                        <p>Book of the month: {ele.botm  === true ? `Yes` : `No`}</p>
                        <p>unique book id: {ele._id}</p>
                      </div>
                  </div>
                </div>
            })}
        </div>
    )
  }
}

export default AllbooksAdmin