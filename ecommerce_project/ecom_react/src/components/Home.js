import React from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import {URL} from '../config'

class Home extends React.Component{

  state = {
    botm : '', 
    botm_img : '', 
    bestsellers : [],
    book_img : '', 
    search : '',
    searchBooks : [],
    email : ''
  }

componentDidMount(){
    this.findBOTM()
    this.findBestsellers()
}

findBOTM = () => {
  axios.get(`${URL}/book/findbotm`)
  .then (res => {
    if (res.data.response !== null ) {
      this.setState ({botm : res.data.response})
      localStorage.setItem('botm_img', res.data.response.image)
    } else {
      this.setState ({ botm : { _id : '5d91b5d7108fe704282c43b6', image : 'guarani-purahei-lopez-martin.jpg' , title : 'Guarani purahéi: Cantos guaraníes (Guarani Songs)', author: 'Cristian David López'}})
      localStorage.setItem('botm_img', 'guarani-purahei-lopez-martin.jpg')
    }
    })
  .catch (error => console.log(error))
}

findBestsellers = async e => {
  axios.get(`${URL}/book/findbestsellers`)
  .then (res => {
    this.setState ({bestsellers : res.data.response})
  })
  .catch (error => console.log(error))
}

handleChange = e => {
  console.log(this.state.search)
  this.setState({search : e.target.value})
}

handleSubmit = async e => {
  e.preventDefault()
  let any = this.state.search
  axios.post(`${URL}/book/findbooksany`, { any }) 
  .then (res => {
    this.props.setStateBooks(res.data.response)
    this.props.history.push('/products')
  })
  .catch (error => console.log(error))
}

handleChange = e => {
  console.log(this.state.search)
  this.setState({search : e.target.value})
}

handleSubmit = async e => {
  e.preventDefault()
  let any = this.state.search
  axios.post(`${URL}/book/findbooksany`, { any }) 
  .then (res => {
    this.props.setStateBooks(res.data.response)
    this.props.history.push('/products')
  })
  .catch (error => console.log(error))
}

handleChange1 = e => {
  console.log(this.state.email)
  this.setState({email : e.target.value})
}

handleSubmit1 = async e => {
  e.preventDefault()
  this.props.history.push({pathname : '/signup', state: { email : this.state.email} })
}

    render () {
      let image;
    if (localStorage.botm_img !== undefined) {
      image = localStorage.botm_img; 
    } else {
      image = 'guarani-purahei-lopez-martin.jpg'
    }
        return (
            <div>
                <div className="splash">
                <header>
                  <div className="headerbox">
                    <p><h2><i className="lrg">LAB // L</i>ATIN <i className="lrg">A</i>MERICAN <i className="lrg">B</i>OOKSTORE</h2></p>
                  </div>
                </header>

                <nav>
                  <div className="dropdown"><Link to="/products" >{'// Books'}</Link>
                    <div className="dropdown-content">
                          <p><div onChange={this.props.findBooksByGenre}>
                            <Link to="/products"><p value='all'>See all</p></Link>
                                {this.props.genres.map((e, i)=> {
                                return e.genre !== "temporary_genre"
                                ? <a href="#"><div onClick={()=>this.props.history.push({pathname:'/products', state : {_id : e._id} })} key={i} value={e._id}>{e.genre}</div></a>
                                : null
                              })}
                          </div></p>
                    </div>
                  </div>
                  <div><a href="#bestsellers">{'// Bestsellers'}</a></div>
                  <div ><Link to="/about" >{'// About'}</Link></div>                  
                  <div><form onSubmit={this.handleSubmit} onChange={this.handleChange} className='homesearch' >
                       <input placeholder="Search title, author or ISBN"/><span onClick={this.handleSubmit}>// Search</span></form></div>
                </nav>

                <div className='content' >
                  <h2>Welcome to the LAB//</h2>
                  <i><p className="welcome">The home of books that share themes related to the South American contient. The LAB was curated to challenge perceptions and expand cultural and critical knowledge of the Hispanic world and beyond.</p></i>
                  <div className='HomeCTAContainer'>
                        <div className='BOMcontent' >
                          <div className='BOMh1'>
                            <i><h1>BOOK</h1></i>
                            <i><h1>OF THE MONTH</h1></i>
                          </div>
                          <div className='BOMtext'>
                            <p><b><Link to={`/product/${this.state.botm._id}`} className="link">{this.state.botm.title}</Link></b></p>
                            <p><b>By {this.state.botm.author}</b></p>
                          </div>
                        </div>
                      <div className='BOMcontent'>
                            <Link to={`/product/${this.state.botm._id}`} onClick={()=>localStorage.setItem('book_img', image)}><img src={require("../images/books/"+image)} className="BOMbook" alt="Book cover, book of the month"></img></Link>
                      </div>
                      <div className='CTAcontent'>
                            <p>Signing Up is simple!</p>
                            <p>Just start with your email</p>
                            <div><form onSubmit={this.handleSubmit1} onChange={this.handleChange1} className='homesignup' >
                                 <input placeholder="example@example.com"/>
                            <p>and we'll take you through the rest //</p>
                            <button>Sign me up</button></form></div>
                      </div>
                    <a name="bestsellers"></a>
                    </div>
                  <div className="cont">
                      <h3> See some of our bestsellers here:</h3>

                      { this.state.bestsellers.map( (ele,i) => {
                              let image = ele.image
                                return (
                                  <div key={i} className="seeallbooks">
                                    <div key={i} className="seeallbooksbox">
                                      <div>
                                        <Link to={`/product/${ele._id}`} onClick={()=>localStorage.setItem('book_img', `${ele.image}`)}><img src={require("../images/books/"+image)} className="bookcover" alt="Book cover"></img></Link>
                                      </div>
                                      <div>                        
                                        <p>{ele.title}</p>
                                        <span>{ele.price}€ </span>
                                        <span><Link to={`/product/${ele._id}`} onClick={()=>localStorage.setItem('book_img', `${ele.image}`)}><button type="button">Find out more</button></Link></span>
                                      </div>
                                  </div>
                                </div>
                                ) 
                        })
                      }
                  </div>
                </div>
            </div>
            </div>
          )
    }
}

export default Home;