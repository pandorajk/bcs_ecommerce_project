import React from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import AllbooksAdmin from "./AllbooksAdmin"
import {URL} from '../config'

class Admin_book extends React.Component{

    state = {
        book_id : '', 
        title : '', 
        author : '',
        publishing : '', 
        isbn_asin : '',
        price : '',
        genre_id : '',
        genre_id2 : '',
        bestseller : '',
        stock : '',
        image : '', 
        blurb: '', 
        botm : '',
        comments : ''
    }

    componentDidMount(){
        if (!this.props.adminAccess ) {
          return this.props.history.push('/login') 
        }
    }

handleChange = e => this.setState({ [e.target.name]: e.target.value})

submitCreate = e => {
    e.preventDefault()
    const { title, author, publishing, isbn_asin, price, genre_id, genre_id2, bestseller, stock, image, blurb, botm, comments } = this.state
    axios.post(`${URL}/book/create`, { title, author, publishing, isbn_asin, price, genre_id, genre_id2, bestseller, stock, image, blurb, botm, comments })
    .then( res => {
        console.log(res.data)
        if( res.data.ok){
            window.localStorage.removeItem("botm_img");
            alert('You added a new book sucessfully')
        }else{
            alert('Not able to add new book')
        }
        
    })
    .catch(error => console.log(error))
}

submitRemove = (e, book_id) => {
    e.preventDefault()
    alert('Book has been deleted')
    axios.post(`${URL}/book/remove`, {book_id})
    .then( res => {
        this.props.findAllGenres()
    })
    .catch(error => console.log(error))
}

    render () {
        return (
            <div className='content'>
                <h2>Admin dashboard - edit book</h2>
                <Link to="/admin"><button type="button">Return to Admin Dashboard</button></Link>
        
                <div className="box">
                    <h3>Add a new Book</h3>
                    <form onSubmit={this.submitCreate}>
                        <p>Title: 
                        <input  name='title' onChange={this.handleChange}/></p>
                        <p>Author: 
                        <input  name='author' onChange={this.handleChange}/></p>
                        <p>Publiser and Year: 
                        <input  name='publishing' onChange={this.handleChange}/></p>
                        <p>ISBN/ASIN (Input: 10 digit number): 
                        <input  name='isbn_asin' onChange={this.handleChange}/></p>
                        <p>Price: 
                        <input  name='price' onChange={this.handleChange}/></p>
                        <p>Primary genre: 
                        <select  onChange={this.handleChange} name='genre_id'>
                            {this.props.genres.map((e, i)=> {
                            return <option  key={i} value={e._id}>{e.genre}</option>})}
                        </select></p>
                        <p>Secondary genre: 
                        <select onChange={this.handleChange} name='genre_id2'>
                            {this.props.genres.map((e, i)=> {
                            return <option key={i} value={e._id}>{e.genre}</option>})}
                        </select></p>
                        <p>Besteller: 
                            <select onChange={ this.handleChange} name='bestseller'>
                                <option>--Select Yes or No</option>
                                <option value="true">Yes</option>
                                <option value="false">No</option>
                            </select></p>
                        <p>Stock (Input: Number): 
                        <input name='stock' onChange={this.handleChange}/></p>
                        <p>Image: (e.g:  guarani-purahei-lopez-martin.jpg ) 
                    {/* Will need to change this to online files such as with Cloudinary ... then code on other pages will need to change to accomodate the url */}
                        <input name='image' onChange={this.handleChange}/></p>
                        <p>Blurb: 
                        <input name='blurb' onChange={this.handleChange}/></p>
                        <p>Book of the Month: 
                            <select  name='botm' onChange={ this.handleChange}>
                                <option>--Select Yes or No</option>
                                <option value="true">Yes</option>
                                <option value="false">No</option>
                            </select></p>
                        <button>Add new book</button>
                    </form>
                </div>

                <div className="box">
                        <h3>Find a book to update</h3>
                        <select onChange={this.props.findBooksByGenre}>
                            <option value='select'>---select books by genre</option>
                            <option value='all'>SEE ALL</option>
                                {this.props.genres.map((e, i)=> {
                                return e.genre !== "temporary_genre" 
                                ? <option key={i} value={e._id}>{e.genre}</option>
                                : null
                                })}
                        </select>

                <AllbooksAdmin {...this.props}
                                allbooks={this.props.books}
                                submitRemove={this.submitRemove}/>
                </div>
            </div>
          )
    }
}

export default Admin_book;