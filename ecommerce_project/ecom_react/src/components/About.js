import React from 'react'

class About extends React.Component{
  render(){
    return (
        <div className="content">
            <header><div className="headerbox" className="otherpages">
                <p><h2><i className="lrg">// A</i>BOUT</h2></p>
                <p className="othertext">The Latin American Bookstore is an project created by Pandora J Knocker.</p> 
                <p className="othertext">The project combines my passions of web development and arts and social sciences literature related to Latin America.</p> 
                <p className="othertext">As a web development project; this website showcases full stack web development skills in React.js, Express, MongoDB and Node.js.</p> 
                <p className="othertext">As an bookstore project; you can discover books that explore and challenge perceptions related to Latin American arts and culture.</p> 
            </div></header>  
            <div className="animalBox2"><p><img src={require("../images/animals/armadillo-flip.jpg")} className="animallrg" alt="armadillo"></img></p></div>      
        </div>
    )
  }
}

export default About