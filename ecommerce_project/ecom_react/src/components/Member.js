import React , { useState, useEffect } from 'react'
import axios from 'axios' 
import { URL } from '../config'

const Member = (props) => {

  const [ member , setMember ] = useState({
        member    : {}
    })

  const [ memberReviews , setMemberReviews ] = useState({
      memberReviews  : [{review : 'none'}]
  }) 
  
  useEffect( () => {
    if (!props.login) { 
      return props.history.push('/login') 
    }
    findMember()
    findReviews()
  } ,[props.login]) //stefano suggested to me to add this empty array : Before we just wanted to run useEffect once. Now, to re-run useEffect, pass a variable (array as syntax), and it will trigger again when this variable is changed.

  const findMember = async e => {
    axios.post(`${URL}/user/findMember`, {email : props.user.email})
    .then (res => {
        setMember(res.data.response)
          let user = {firstname : res.data.response.firstname, email : res.data.response.email, user_id : res.data.response._id}
          props.userEmail(user)
        })
    .catch (error => console.log(error))
  }

  const findReviews = () => {
    let memberReviews = [];
    let idxCount = 0
    axios.get(`${URL}/review/findreviews`)
    .then (res => {
        res.data.response.map((ele, i)=> {
            if (ele.user_id === props.user.user_id) { 
                memberReviews.push(ele);
                }
            })
        res.data.response.map((ele, i)=> {
                if (ele.user_id === props.user.user_id) {
                    idxCount += 1
                    }
                })
        if (idxCount >=1 ) {
          setMemberReviews({memberReviews})
        } else{
          setMemberReviews({memberReviews : [{review : 'You have not reviewed any books yet'}] }) 
        }
    })
    .catch (error => console.log(error))
}

  const deleteReview = (_id) => { //To improve: this should be on App.js - along with findAllReviews/findReviews
    axios.post(`${URL}/review/removereview`, {_id} )
    .then( res => {
        alert(`You have deleted a review`)
        findReviews()
    })
    .catch(error => console.log(error))
  }

if (memberReviews.memberReviews[0].review === 'none') {
  return <div><p>loading</p></div>
} else {

return <div className='content'>
                <p><button onClick={()=>{props.logout(); props.history.push('/')}}>Logout</button></p>
                <p>{props.adminAccess 
                  ? <button onClick={()=>props.history.push('/admin') }>Admin Area</button>
                  : null}</p>
            <div className='memberpage'>
                <div>
                  <img src={require("../images/anon-profile3.jpeg")} className='memberpic' alt="Anon profile" />
                </div>
                <div className="memberinfo">
                  <h1><i>//</i> Hello {member.firstname}!</h1>
                  <p>Email : {member.email}</p>
                </div>
              </div>
          <h3>Your book reviews:</h3>

          <div>{memberReviews.memberReviews.map((e,i)=>{
                  let imageId;
                  let title;
                      props.books.map((ele, indx)=> {
                        if (e.book_id === ele._id) {
                          imageId = ele.image
                          title = ele.title
                        }
                      })
                  if (e.book_id === undefined) {
                    return <div key={i}><p>{e.review}</p></div>
                  } else { 
                    return  <div key={i} className="memberReview">
                              <div>
                                <img src={require(`../images/books/${imageId}`)} className="bookcoverbasket" alt="book"></img>
                              </div>
                              <div>
                                <p>{title}</p>
                                <p><i>//</i>"{e.review}"</p>
                                <button onClick={()=> {if (window.confirm('Are you sure you wish to delete this review?')) deleteReview(e._id) } }> Delete review </button>
                                </div> 
                              </div>
                }
              })}</div>
          <div className="animalBox"><p><img src={require("../images/animals/armadillo.jpg")} className="animallrg" alt="armadillo"></img></p></div>
          </div>
  }
}

export default Member