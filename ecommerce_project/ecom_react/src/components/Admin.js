// import React from "react";
import React , { useEffect } from 'react'
import { Link } from "react-router-dom";
import { NavLink } from "react-router-dom";

const AdminDashboard = (props) => {

  useEffect( () => {
      if (!props.adminAccess ) {
        return props.history.push('/login') 
      }
   })

  return  <div className='content'>
          <button onClick={()=>{localStorage.removeItem('token');props.history.push('/')}}>logout</button>   
          <p><NavLink to={"/member"}>Back to Member Profile</NavLink></p>
          <h2>Admin dashboard</h2>

          <div className="admindashboard">
            <div className="box">
            <h3>GENRES // Create, remove or update a book genre</h3>
            <Link to="/admin/genre"><button type="button">Genre update</button></Link>
            </div>

            <div className="box">
            <h3>BOOKS // Create, remove or find a book to update</h3>
            <Link to="/admin/book"><button type="button">Book update</button></Link>
            </div>

            <div className="box">
            <h3>REVIEWS // Create, remove or find a book to update</h3>
            <Link to="/admin/review"><button type="button">Review update</button></Link>
            </div>
          </div>
     </div>        
  }

export default AdminDashboard




