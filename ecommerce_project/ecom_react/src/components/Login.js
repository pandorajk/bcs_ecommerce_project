// import React from "react";
import React , { useState } from 'react'
import { NavLink } from "react-router-dom";
import axios from 'axios' 
import {URL} from '../config'

const Login = (props) => {

	const [ form , setValues ] = useState({
		email    : '',
        password : ''
    })
    
    const [ message , setMessage ] = useState('')
    
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try{
            const response = await axios.post(`${URL}/user/login`,{
            email:form.email,
                password:form.password
            })
            if( !response.data.ok ){
            return setMessage(response.data.message)
            }
            if( response.data.token ){
            localStorage.setItem('token',JSON.stringify(response.data.token)) 
            const verified = await props.verify_token()
            setMessage(verified.ok ? response.data.message : verified.message)
            if(verified.ok){
                return setTimeout( ()=> props.history.push({pathname : '/member', state : props.user }),2000)   
            }
            }

        }
        catch(error){
            console.log(error)
        }
    }

    const showOrHidePassword = e => {
        const togglePassword = document.getElementById("togglePassword");
        togglePassword.addEventListener("change", showOrHidePassword);
        const password = document.getElementById('password');
        if (password.type === 'password') {
          password.type = 'text';
        } else {
          password.type = 'password';
        }
      };

	return <div className="signup">
            <div className='content'>
                <form onSubmit={handleSubmit}
                    onChange={handleChange}
                    className='form_container'>
                <h2>Login with your email and password to discover the benefits of being a member</h2>
                <p><label>Email</label></p>    
                <p><input name="email"/></p>

                <p><label name="password">Password</label></p>
                <p><input name="password" id="password" onChange={showOrHidePassword}/></p>
                <p><label name="togglePassword">show password</label>
                <input type="checkbox" id="togglePassword" className="checkbox"/></p>

                <p><button>login</button></p>
              </form>
                <p><img src={require("../images/animals/tapir.png")} className="animalsml" alt="tapir"></img></p>
                <div className='message'><h4>{message}</h4></div>

            </div>
         </div>
}

export default Login



