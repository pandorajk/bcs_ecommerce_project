// import React from "react";
import React from "react";

class OrderConfirmation extends React.Component{
    render() {
        console.log(this.props)
        return (
            <div className="content">
                <h1>Order confirmation</h1>
                <h2>Many thanks for purchasing a book with the <i>//</i> LAB</h2>
                <div className="animalBox">
                   <p><img src={require("../images/animals/anteater.jpg")} className="animallrg" alt="anteater"></img></p>
                </div>
            </div>
        )
    }
}

export default OrderConfirmation;