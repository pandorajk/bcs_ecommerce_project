import React, { Component } from "react";
import {
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement,
  injectStripe,
  StripeProvider,
  Elements } from "react-stripe-elements";
import axios from "axios";
import {URL} from '../config'

const basket = JSON.parse(localStorage.getItem('basket'))

class CheckoutForm extends Component {
  state = {
    errorMessage: "",
    cardNumber: false,
    cardExpiry: false,
    cardCvc: false,
    amount: this.props.total.location.state, 
  };

  handleChange = ({ elementType, complete, error }) => {
    if (error) return this.setState({ errorMessage: error.code });
    return this.setState({ [elementType]: complete });
  };

  handleInputChange = e => this.setState({ [e.target.name]: e.target.value });

  handleSubmit = async e => {
    e.preventDefault();
    const { cardNumber, cardCvc, cardExpiry } = this.state;
    if (!cardNumber || !cardCvc || !cardExpiry) return alert("Please fill all the fields");
    const fullname = this.state.name + this.state.lastname;
    const { name, lastname, email, pc, amount } = this.state;
    if (this.props.stripe) {
      const { token } = await this.props.stripe.createToken({ name:fullname, email });
      const response = await axios.post(`${URL}/payment/charge`, {
          token_id: token.id,
          amount,
          name,
          lastname,
          email,
          pc
        });
        if (response.data.status === "succeeded") {
            alert("Payment successful") 
            const basket = JSON.parse(localStorage.getItem('basket'))
            basket.map((ele,i)=>{
                      let _id = ele._id;
                                  console.log('qty?',ele.qty)
                                  console.log('stock?', ele.stock)
                      let newStock = ele.stock - ele.qty;
                      this.setState({_id  : newStock })
                                  console.log('newstock?', newStock)
                          axios.post(`${URL}/book/updatestock`, {  _id, newStock})
                          .then( res => {
                          console.log(res); })
                          .catch(error => console.log(error));
                      })
            localStorage.removeItem('basket')
            this.props.total.history.push({pathname:'/order_confirmation'})
        } else {
            alert("Payment error")
        } 

    } else {
      alert("Stripe.js hasn't loaded yet.");
    }
  };

  render() {
    return ( <div>
      <form className="checkout" onSubmit={this.handleSubmit} >
        {/*************************** FIRST ROW ****************************/}
        <div className="split-form">
          <label>
            Name
            <p><input required name="name" type="text" placeholder="Jane" className="StripeElement" onChange={this.handleInputChange} /></p>
          </label>
          <label>
            Lastname
            <p><input required name="lastname" type="text" placeholder="Doe" className="StripeElement" onChange={this.handleInputChange} /></p>
          </label>
        </div>
        {/*************************** SECOND ROW ****************************/}
        <div className="split-form">
          <label>
            Email
            <p><input
              required
              name="email"
              type="email"
              placeholder="jane.doe@example.com"
              className="StripeElement"
              onChange={this.handleInputChange}
            /></p>
          </label>
        </div>
        {/***************************** THIRD ROW *****************************/}
        <div className="split-form" >
          <label>
            Card number
            <div className="mockInput"><CardNumberElement onChange={this.handleChange} /></div>
          </label>
          <label>
            Expiration date
            <div className="mockInput"><CardExpiryElement onChange={this.handleChange} /></div>
          </label>
        </div>
        {/*************************** FOURTH ROW ****************************/}
        <div className="split-form">
          <label>
            CVC
            <div className="mockInput"><CardCVCElement onChange={this.handleChange} /></div>
          </label>
          <label>
            Postal code
            <p><input
              name="pc"
              type="text"
              placeholder="08002"
              className="StripeElement"
              onChange={this.handleChangeInput}
            /></p>
          </label>
        </div>
        <div className="error" role="alert">
          {this.state.errorMessage}
        </div>
        <button>Pay {this.state.amount} €</button>
      </form> 
      </div>
    );
  }
}

export default injectStripe(CheckoutForm);
