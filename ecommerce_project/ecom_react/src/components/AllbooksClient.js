import React from 'react'
import { Link } from "react-router-dom";

class Allbooks extends React.Component{
  render(){
    return (
        <div>
            {this.props.allbooks.map((ele,i) => {
              let image = ele.image
            return <div key={i} className="seeallbooks">
                    <div key={i} className="seeallbooksbox">
                    <div>
                      <Link to={`/product/${ele._id}`} onClick={()=>localStorage.setItem('book_img', `${ele.image}`)} ><img src={require("../images/books/"+image)} className="bookcover" alt="Book cover"></img></Link>
                    </div>
                    <div>                        
                      <p>{ele.title}</p>
                      <span>{ele.price}€ </span>
                      <span><Link to={`/product/${ele._id}`} onClick={()=>localStorage.setItem('book_img', `${ele.image}`)} ><button type="button">Find out more</button></Link></span>
                    </div>
                    </div>
                  </div>
            })}
        </div>
    )
  }
}

export default Allbooks