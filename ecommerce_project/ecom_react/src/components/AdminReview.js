import React from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import {URL} from '../config'

class Admin_review extends React.Component{

    state = {
        reviews : []
    }

componentDidMount(){
    if (!this.props.adminAccess ) {
        return this.props.history.push('/login') 
    }
  this.findAllReviews()
}
    
findAllReviews = () => {
    axios.get(`${URL}/review/findreviews`)
    .then (res => {
        this.setState({reviews : res.data.response})
    })
    .catch (error => console.log(error))
    }

deleteReview = (_id) => {   //To improve: this should be on App.js - along with findAllReviews/findReviews
    axios.post(`${URL}/review/removereview`, {_id} )
    .then( res => {
        alert(`You have deleted a review`)
        this.findAllReviews()
    })
    .catch(error => console.log(error))
    }

    render () {
        console.log('rvs state in admin review ',this.state.reviews)
        console.log('props',this.props)
        return (
            <div className='content'>

                <h2>Admin dashboard - edit reviews</h2>
                <Link to="/admin"><button type="button">Return to Admin Dashboard</button></Link>

                <div className="box">
                    <h4>Remove a review:</h4>
                    <div>{ this.state.reviews.map((e,i)=>{
                        let imageId;
                        let title;
                                this.props.books.map((ele, indx)=> {
                                    if (e.book_id == ele._id) {
                                    imageId = ele.image
                                    title = ele.title
                                    }
                                })
                        return <div key={i} className="memberReview">
                                    <div>
                                        <img src={require(`../images/books/${imageId}`)} className="bookcoverbasket" alt="bookcover"></img>
                                    </div>
                                    <div>
                                       <p>{e.firstname} (User ID: {e.user_id}) wrote:</p>
                                       <p><b>"{e.review}"</b></p>
                                       <button onClick={()=> {if (window.confirm('Are you sure you wish to delete this review?')) this.deleteReview(e._id) } }> Delete review </button>
                                    </div>
                                </div>
                       })   
                    }</div>
                </div>

            </div>
          )
    }
}

export default Admin_review;