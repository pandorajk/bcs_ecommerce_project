import React from "react";
import AllbooksClient from "./AllbooksClient"
import { Link } from "react-router-dom";

class Products extends React.Component{

componentDidMount(){
    if (this.props.location.state !== undefined) {
            this.props.findBooksByGenre(this.props.location.state._id)
    } 
    //LEARNING NOTE: Here this function means that the page will render, and then trigger this function. This function has taken props from App.js line 58: this.props.history.push({pathname:'/products'..... in which we passed (push) the _id as props  to the path /products (i.e. right here!)
}

componentWillUnmount() {
    this.props.findAllBooks()
}

  render(){
    return (
        <div className='content'>
            <header><div className="headerbox" className="otherpages" className="discoverimage">
                <p><h2><i className="lrg">// D</i>ISCOVER</h2></p>
                <p className="othertext">See all of our books displayed below or search by genre.</p> 
            </div></header>
            <nav className="nav">
                <div value='all' onClick={()=>this.props.findBooksByGenre('all')}><b> //See all products</b></div>
                <div className="dropdown"><Link to="/products" >{'// Search by Genre'}</Link>
                    <div className="dropdown-content">
                          <p><div> 
                                {this.props.genres.map((e, i)=> {
                                return e.genre !== "temporary_genre"
                                ? <a href="#"><div onClick={()=>this.props.findBooksByGenre(e._id)} key={i} value={e._id}>{e.genre}</div></a>
                                : null
                              })}
                          </div></p>
                    </div>
                  </div>
            </nav>
            {this.props.books.length === 0 ? <p>Sorry there were no books matching your search - although you can discover our books with the options above</p> : <AllbooksClient allbooks={this.props.books}/> }
      </div>
        )
    }
}

export default Products


