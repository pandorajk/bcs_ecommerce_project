import React, { Component } from "react";
import { Elements, StripeProvider } from "react-stripe-elements";
import CheckoutForm from "./CheckoutForm";
import {pk_test} from "../config";
import { Link } from "react-router-dom";

class Payment extends React.Component{
    render() {
        return (
            <div className="content">
            <h1>Payment</h1>
            <button><Link to="/basket">Back to the basket</Link></button>
            <StripeProvider apiKey={ pk_test }>
                    <div className="example">
                    <Elements>
                        <CheckoutForm total={this.props}/>
                    </Elements>
                    </div>
                </StripeProvider>
            </div>
        );
    }
};

export default Payment;