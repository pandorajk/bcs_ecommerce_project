import React from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
import {URL} from '../config'

// *Create new genre should clear input field after it's been added - and then display the new genre and it's ID - like when you select it in the box above.

class Admin_genre extends React.Component{

    state = {
        genre : '', 
        updateGenre : '',
        findGenre : {}
    }

componentDidMount(){
    if (!this.props.adminAccess ) {
      return this.props.history.push('/login') 
    }
}

changeGenreCreate = e => this.setState({ genre : e.target.value})

submitCreate = e => {
    e.preventDefault()
    const {genre} = this.state
    axios.post(`${URL}/genre/create`, {genre})
    .then( res => {
        alert(`Success! You added a new genre: "${genre}"`)
    })
    .catch(error => console.log(error))
}

changeGenreRemove = e => this.setState({ genre : e.target.value})

submitRemove = e => {
//If time - add server-side function where books in a removed genre are saved to temporary_genre
    const {genre} = this.state
    genre === 'temporary_genre'
    ? alert('You cannot delete genre called "temporary_genre"')
    : axios.post(`${URL}/genre/remove`, {genre})
    .then( res => {
        alert(`You have deleted a genre: "${genre}"`)
        this.props.findAllGenres()

    })
    .catch(error => console.log(error))
}

changeGenreUpdate1 = e => this.setState({ genre : e.target.value})
changeGenreUpdate2 = e => this.setState({ updateGenre : e.target.value})

submitUpdate = e => {
    const {genre, updateGenre} = this.state
    genre === 'temporary_genre'
    ? alert('You cannot change genre called "temporary_genre"')
    : axios.post(`${URL}/genre/updategenre`, {genre, updateGenre})
}

    render () {
        console.log(this.props)
        return (
            <div className='content'>
                <h2>Admin dashboard - edit genre</h2>
                <Link to="/admin"><button type="button">Return to Admin Dashboard</button></Link>

                <div className="box">
                    <h4>Create a new Genre</h4>
                    <p>Enter name of new Genre to be ADDED:</p>
                    <form onSubmit={this.submitCreate}>
                        <input name='genreCreate' onChange={this.changeGenreCreate}/>
                        <button>Submit new genre</button>
                    </form>
                </div>

                <div className="box">
                    <h4>Remove a Genre</h4>
                    <p>Enter the name of Genre to be REMOVED: </p>
                    <form onSubmit={() => {if (window.confirm('Are you sure you wish to delete this genre?')) this.submitRemove() } }> 
                        <input name='genreRemove' onChange={this.changeGenreRemove}/>
                        <button>Remove genre</button>
                    </form>
                </div>

                <div className="box">
                    <h4>Update a Genre</h4>
                    <form onSubmit={this.submitUpdate}>
                        <p>Enter the name of current Genre to be UPDATED...(casesensitive)</p>
                        <input name='genreUpdate' onChange={this.changeGenreUpdate1}/>
                        <p>...then enter the new name: </p>
                        <input name='genreUpdate' onChange={this.changeGenreUpdate2}/>
                        <button>Update genre</button>
                    </form>
                </div>
    
                <div className="box">
                    <h4>Select a genre to see it's ID</h4>
                        <select onChange={this.props.findGenreByGenreID}>
                            <option value='select'>---select from this list</option>
                            {this.props.genres.map((e, i)=> {
                            return <option key={i} value={e._id}>{e.genre}</option>})}
                            <option value='all'>See all</option>
                        </select>
                        <p>{this.state.findGenre.genre}</p>
                        <p>{this.state.findGenre._id}</p>
                        {
                            this.props.genres.map( (ele,i) => {
                                return (
                                    <div>
                                        <p>{ele.genre}, Unique ID :{ele._id} </p>
                                    </div>
                                )
                                
                            })
                        }
                </div>
            </div>
          )
    }
}

export default Admin_genre;