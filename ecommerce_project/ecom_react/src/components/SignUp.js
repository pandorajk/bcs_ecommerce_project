// import React from "react";
import React , { useState, useEffect } from 'react'
import axios from 'axios' 
import { NavLink } from "react-router-dom";
import { URL } from '../config'

const SignUp = (props) => {

	const [ form , setValues ] = useState({
    firstname : '',
    lastname : '',
		email    : '',
		password : '',
		password2: ''
  })

  useEffect( () => {
     if( props.location.state !== undefined){
      setValues({...form, email : props.location.state.email})
     } else {
      setValues({...form, email : ''})
     }    
  } ,[])
  
	const [ message , setMessage ] = useState('')
	const handleChange = e => {
       setValues({...form,[e.target.name]:e.target.value})
  }
  
	const handleSubmit = async (e) => {
		e.preventDefault()
		try{
			const response =  await axios.post(`${URL}/user/signup`,{
              firstname : form.firstname, 
              lastname : form.lastname,
              email    : form.email,
              password : form.password,
              password2: form.password2
	          })
          setMessage(response.data.message)
          props.history.push("/login")
		}
		catch( error ){
			console.log(error)
		}

  }
  
	return <div className="signup">
          <div className='content' >
              <h2>Signing up is easy!</h2>
              <form onSubmit={handleSubmit}
                    onChange={handleChange}
                    className='form_container'>
              <p><label>Name</label></p>
              <p><input name="firstname"/></p>
              <p><label>Lastame</label></p>
              <p><input name="lastname"/></p>
              <p><label>Email</label></p>
              <p><input value={form.email} name="email"/></p>
              <p><label>Password</label></p>
              <p><input name="password"/></p>
              <p><label>Repeat password</label></p>
              <p><input name="password2"/></p>
              <p><button>Sign me up!</button></p>
              <div className='message'><h4>{message}</h4></div>
              </form>
              <p><img src={require("../images/animals/rhea.png")} className="animalsml" alt="rhea"></img></p>
            <NavLink to={"/login"}>Already a member? Click here to login</NavLink>
          </div>
        </div>
}

export default SignUp
