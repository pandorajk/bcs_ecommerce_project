import React from 'react'
import axios from 'axios';
import {URL} from '../config'
import { Link } from "react-router-dom";

class Updatebook extends React.Component{
    state = {
        book_id : '', 
        title : '', 
        author : '',
        publishing : '', 
        isbn_asin : '',
        price : '',
        genre_id : '',
        genre_id2 : '',
        bestseller : '',
        stock : '',
        image : '', 
        blurb: '', 
        botm : ''
    }

handleChange = e => this.setState({ [e.target.name]: e.target.value})

async componentDidMount(){      
    try{
        const response = await axios.post(`${URL}/book/findbookbyID`, { book_id : this.props.location.state._id})
        this.setState({ book_id : response.data.response._id, 
                        title : response.data.response.title, 
                        author : response.data.response.author, 
                        publishing : response.data.response.publishing,
                        isbn_asin : response.data.response.isbn_asin,
                        price : response.data.response.price,
                        genre_id : response.data.response.genre_id,
                        genre_id2 : response.data.response.genre_id2,
                        bestseller : response.data.response.bestseller,
                        stock : response.data.response.stock,
                        image : response.data.response.image, 
                        blurb : response.data.response.blurb, 
                        botm : response.data.response.botm
                       }, ()=> console.log(this.state))
    }
    catch (err) {}
}

submitUpdate = e => {
    alert('You are about to update a book')
    axios.post(`${URL}/book/update`, this.state)
    .then( res => {
        console.log(res)
    })
    .catch(error => console.log(error))
}

  render(){
    return (
        <div className="content">
        <p><Link to="/admin/book"><button type="button">Return to admin - edit book area</button></Link></p>
        <p><h3>// Input new data into the fields you wish to update: </h3></p>
        <form onSubmit={this.submitUpdate}>
            <p><b>Title:</b>
            <input placeholder="Title"
                        name='title' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.title}</p>
            <p><b>Author:</b>      <input placeholder="Author"
                        name='author' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.author}</p>
            <p><b>Publishing:</b>      <input placeholder="publishing"
                        name='publishing' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.publishing}</p>
            <p><b>ISBN/ASIN:</b>      <input placeholder="isbn_asin"
                        name='isbn_asin' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.isbn_asin}</p>
            <p><b>Price:</b>      <input placeholder="price"
                        name='price' 
                        onChange={this.chhandleChange}/></p>
                        <p>{this.state.price}€</p>
            <p><b>Primary genre:</b>      <select onChange={this.handleChange} name='genre_id' >
                            {this.props.genres.map((e, i)=> {
                            return <option key={i} value={e._id}>{e.genre}</option>})}
                        </select></p>
            <p><b>Secondary genre:</b>      <select onChange={this.handleChange} name='genre_id2'>
                            {this.props.genres.map((e, i)=> {
                            return <option key={i} value={e._id}>{e.genre}</option>})}
                        </select></p>
            <p><b>Bestseller:</b>      <select onChange={ this.handleChange} name='bestseller' >
                                <option>--Select Yes or No</option>
                                <option value="true">Yes</option>
                                <option value="false">No</option>
                            </select></p><p>{this.state.bestseller === true ? 'Yes' : 'No'}</p>
            <p><b>Stock:</b>      <input placeholder="stock"
                        name='stock' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.stock}</p>
            <p><b>Image URL:</b>      <input placeholder="image_url"
                        name='image' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.image}</p>
            <p><b>Blurb:</b>      <input placeholder="blurb"
                        name='blurb' 
                        onChange={this.handleChange}/></p>
                        <p>{this.state.blurb}</p>
            <p><b>Book of the Month: </b>     <select onChange={ this.handleChange} name='botm'>
                                <option>--Select Yes or No</option>
                                <option value="true">Yes</option>
                                <option value="false">No</option>
                            </select></p><p>{this.state.botm === true ? 'Yes' : 'No'}</p>
        <button>Update book</button>
        </form>
        </div>
    )
  }
}

export default Updatebook