import React from 'react'
import axios from 'axios'
import {URL} from '../config'
import { Link } from "react-router-dom";

class Basket extends React.Component{
state = {
    basket:[{ _id : '', 
              book_title : '', 
              qty : 0, 
              stock : ''
              }], 
    bookinfo : [], 
    total : ''
}

componentDidMount(){
    this.findBooksByID()
}

basketUpdate = (_id, i, action) => {
    let basket = JSON.parse(localStorage.getItem('basket'))
    if ( action === 'increase' ){      
        if (this.state.bookinfo[i].qty == this.state.bookinfo[i].stock-1  || this.state.bookinfo[i].qty >= this.state.bookinfo[i].stock ) {
            alert('Sorry - you have cannot add more - you have reached the stock limit')        // async issue!!!!!!
        } else {
            this.state.bookinfo[i].qty +=1
            //NOTE: also should minus TEMPORARILY from the total qty - so that two people cannot buy the book at the same time. For a set amount of time and then be deleted and 
        }    
    basket[i].qty += 1
    }
    if ( action === 'decrease' ){ 
        if (basket[i].qty > 1) {
            basket[i].qty -= 1
        }
        if (this.state.bookinfo[i].qty > 1) {
            this.state.bookinfo[i].qty -=1} 
        }
        localStorage.setItem( 'basket', JSON.stringify(basket))
        this.setState({basket})
}

itemRemove = (i) => {
    // 1. NB i refers to index in this.state.bookinfo - which is not the same position/index in localstoragebasket
    let basket = JSON.parse(localStorage.getItem('basket'))
    const temp = this.state.bookinfo.splice(i, 1)
    basket = this.state.bookinfo
    localStorage.setItem( 'basket', JSON.stringify(basket))
    // 2. NB state update and refresh page
    this.setState({basket})
}

findBooksByID = () => {
    const bookinfo = [];
    let basket = JSON.parse(localStorage.getItem('basket'))
    if (basket !== null ) {
        basket.map((elem, i)=>{
            axios.post(`${URL}/book/findbookbyID`, { book_id : elem._id}) 
            .then (res => {
              bookinfo.push({_id: res.data.response._id, book_title: res.data.response.title, image: res.data.response.image, price: res.data.response.price, stock: res.data.response.stock, qty : basket[i].qty})
              this.setState ({ bookinfo })
            })
            .catch (error => console.log(error))
        })
    } 
}

totalArr = []

total = () => {
    const length = this.state.bookinfo.length
    const splice = this.totalArr.splice(0, this.totalArr.length - length)

    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    const decimaltotal = this.totalArr.reduce(reducer, 0)

    const gt = parseFloat(decimaltotal).toFixed(2);
    return gt
}

  render(){
    return (<div className="content">
                <h1>Basket</h1>
                <button><Link to="/products">Back to all products</Link></button>
                    {this.state.bookinfo.map((ele, i) => {
                        let _id = ele._id
                        let book_title = ele.book_title
                        let qty = ele.qty
                        let price = ele.price
                        let image = ele.image
                        return  (<div key={i}>
                                    <h4>You added: </h4>
                                    <div key={i} className="basketbox">
                                        <div>
                                            <img src={require("../images/books/"+image)} className="bookcoverbasket" alt="Book cover"></img>                                    
                                        </div>
                                        <div>
                                            <p>{book_title}</p>
                                            <span>Quantity: {qty}</span>
                                            <span><button onClick={()=>this.basketUpdate(_id, i, 'increase')}> + </button></span> 
                                            <span><button onClick={()=>this.basketUpdate(_id, i, 'decrease')}> - </button></span>
                                            <span><button onClick={()=>this.itemRemove(i)}>Remove </button></span>
                                            <p>Price : {price}</p>
                                            <p>TOTAL for product : {price * qty}euros</p>
                                            <h6>{this.totalArr.push(price * qty)}</h6>
                                        </div>
                                    </div>
                                </div>)
                        })}
                    <h3>GRAND TOTAL: {this.total()} euros</h3>
                    <button><Link to="/products" onClick={()=>localStorage.removeItem('basket')}>Clear basket</Link></button>
                    <button onClick={()=>this.props.history.push({pathname:'/payment', state : this.total() })}>Checkout</button>
            </div>
    )
    }
}

export default Basket