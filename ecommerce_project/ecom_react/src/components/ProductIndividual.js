import React from "react";
import axios from 'axios';
import { URL } from '../config'
import { Link } from "react-router-dom";
// import { notDeepEqual } from "assert";

class Product extends React.Component{

    state = {
        book : {},
        review : '', 
        reviews : ['no_review']
    }

componentDidMount(){
    this.findBookByID()
    this.findReviews()
}

componentWillUnmount() {
    localStorage.removeItem("book_img");
}

findBookByID = e => {
    axios.post(`${URL}/book/findbookbyID`, {book_id : this.props.match.params.book_id})
    .then (res => {
        this.setState ({book : res.data.response})})
    .catch (error => console.log(error))
}

basketToLocalStorage = () => {
    const basket = JSON.parse(localStorage.getItem('basket'));
    if (basket != null) {
        const index = basket.findIndex(ele => ele._id === this.state.book._id);
        if (index === -1) {
            basket.push({
            _id : this.state.book._id,
            book_title : this.state.book.title,
            qty : 1,
            stock : this.state.book.stock
            });
          localStorage.setItem('basket', JSON.stringify(basket)) 
        } else {
            basket[index].qty += 1
            localStorage.setItem('basket', JSON.stringify(basket)) 
        }
    } else {
        localStorage.setItem( 'basket', JSON.stringify([{
                                        _id : this.state.book._id,
                                        book_title : this.state.book.title,
                                        qty : 1, 
                                        stock : this.state.book.stock
        }]));
    }
    this.props.history.push("/basket")
}

findBookByID = e => {
    axios.post(`${URL}/book/findbookbyID`, {book_id : this.props.match.params.book_id})
    .then (res => {
        this.setState ({book : res.data.response})})
    .catch (error => console.log(error))
}

handleChange = e => {
    console.log(this.state)
    this.setState({review : e.target.value})
}

handleSubmit = async e => {
    e.preventDefault()
    console.log('props to submit a book', this.props)
    axios.post(`${URL}/review/review`, { user_id : this.props.user.user_id, firstname : this.props.user.firstname, book_id : this.props.match.params.book_id, review : this.state.review})
    .then (res => { 
        console.log(res)
        if (res.data.ok) {
            alert('Thanks for your review!')
            this.findReviews()
        }
    })
    .catch (error => console.log(error))
}

findReviews = () => {
    let reviews = [];
    let idxCount = 0
    axios.get(`${URL}/review/findreviews`)
    .then (res => {
        res.data.response.map((ele, i)=> {
            if (ele.book_id == this.props.match.params.book_id) {
                reviews.push(ele);
                }
            })
        res.data.response.map((ele, i)=> {
                if (ele.book_id == this.props.match.params.book_id) {
                    idxCount += 1
                    }
                })            
        if (idxCount >=1 ) {
            this.setState ({reviews})
        } else{
            this.setState({reviews : ['no_review']}) 
        }
    })
    .catch (error => console.log(error))
}


    render () {
        let image = localStorage.book_img
        return (
            <div className="content">
                        <div className="condor">
                        <button><Link to="/products">Back to all products</Link></button>
                            <header><div className="headerbox" className="otherpages">
                            <p><h2><i className="lrg">// R</i>EAD</h2></p>
                            </div></header>
                            <div className="individual-bookbox">
                                <div>
                                    <img src={require("../images/books/"+image)} className="individual-bookcover" alt="Book cover"></img>
                                </div>
                                <div>
                                    <h2>{this.state.book.title}</h2>
                                    <p className="ibook"><b><i>By {this.state.book.author}</i></b></p>
                                    <p className="ibook">{this.state.book.publishing}</p>
                                    <p>***</p>
                                    <p className="ibook">"{this.state.book.blurb}"</p>
                                    <p className="ibook">ISBN-10/ASIN: {this.state.book.isbn_asin}</p>
                                    <p className="ibook">{this.state.book.price} €</p>
                                    <button onClick={()=>this.basketToLocalStorage()}>Add to basket</button>
                                </div>
                            </div>
                        </div>
                        <div className="review-section">
                            <div>
                                <h2>Reviews:</h2>
                                <div>{this.state.reviews.map((ele,i)=>{
                                        if ( ele == 'no_review' ) {
                                            return <div><p>Oops no-one has written a review for this book yet. Be the first when you Sign Up or Login!</p></div>
                                        } 
                                        else {
                                            return <div key={i}><h3><i>//</i> {ele.firstname}</h3>
                                                <p>{ele.review}</p></div>
                                        }
                                        })
                                }</div>
                            </div>
                                    <div className="writerev-container">        
                                    <div><h2><i>//</i> Write a review</h2></div> 
                                    <div>{this.props.login 
                                        ? <div><form onSubmit={this.handleSubmit} onChange={this.handleChange} className='review-container'>
                                                <input name="review"/><p><button>Submit review</button></p></form></div>          
                                        : <p>Login to write a review</p> }</div> 
                            </div>
                        </div>
            </div>
          )
    }
}

export default Product;