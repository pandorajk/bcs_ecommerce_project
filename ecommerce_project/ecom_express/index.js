//================== SET UP CHECKLIST =====================
// - express
// - app
// - mongoose
// - bodyParser
// - cors
// - routes defined (app.use)
// - app.listen
const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      mongoose = require('mongoose')

// =================== Initial Settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors())

mongoose.set('useCreateIndex', true)

const config = require("./config");  // refer to as config.sk_test   not just sk_test*******
const stripe = require("stripe")(config.sk_test);

async function connecting(){
    try {
        await mongoose.connect('mongodb://127.0.0.1/ecom_db', { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
    }
connecting()

app.use('/book', require('./routes/book.routes'))
app.use('/genre', require('./routes/genre.routes'))
app.use('/review', require('./routes/review.routes'))
app.use('/payment', require('./routes/payment.routes'))
app.use('/user', require('./routes/user.routes'))
// app.use('/email', require('./routes/email.routes'))
 
app.listen(3030, () => console.log(`listening on port 3030`))



