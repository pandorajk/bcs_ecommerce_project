const router = require('express').Router()
const controller = require('../controllers/book.controllers')

    router.post('/create', controller.create)
    router.post('/remove', controller.remove)
    router.post('/update', controller.update)
    router.post('/updatestock', controller.updateStock)

    router.get('/findallbooks', controller.findAllBooks)
    router.get('/findbotm', controller.findBOTM)
    router.get('/findbestsellers', controller.findBestsellers)
    
    router.post('/findbooksbygenre', controller.findBooksByGenre)
    router.post('/findbookbytitle', controller.findBookByTitle)
    router.post('/findbookbyID', controller.findBookByID)
    router.post('/findbooksbyID', controller.findBooksByID)
    router.post('/findbooksany', controller.findBooksAny)

module.exports = router