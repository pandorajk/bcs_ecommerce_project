const router = require('express').Router()
const controller = require('../controllers/genre.controllers')

    router.post('/create', controller.create)
    router.post('/remove', controller.remove)
    router.post('/updategenre', controller.update)

    router.get('/findallgenres', controller.findAllGenres)
    
    router.post('/findgenrebygenreID', controller.findGenreByGenreID)

module.exports = router