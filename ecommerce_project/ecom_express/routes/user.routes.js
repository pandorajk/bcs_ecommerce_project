const router = require('express').Router();
const controller = require('../controllers/user.controllers')

    router.post('/signup',controller.signup);
    router.post('/login',controller.login);
    router.post('/verify_token',controller.verify_token);
    router.post('/findMember',controller.findMember);

    //     router.post('/update', controller.update)
    //     router.post('/orders/:user_id', controller.orders)
    //     router.post('/read/', controller.read)
    //     router.post('/review/', controller.review)

    //     router.get('/findall', controller.findAll)
    //     router.get('/findone/:_id', controller.findOne)

    router.post('/update_admin',controller.update_admin);

module.exports = router