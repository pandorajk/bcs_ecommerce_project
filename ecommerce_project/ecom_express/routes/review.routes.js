const router = require('express').Router()
const controller = require('../controllers/review.controllers')

    router.post('/review', controller.createReview)
    router.get('/findreviews', controller.findReviews)
    router.post('/removereview', controller.removeReview)

module.exports = router