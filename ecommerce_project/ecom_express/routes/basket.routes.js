const router = require('express').Router()
const controller = require('../controllers/payment.controllers')

    router.post('/basket/checkstock', controller.basketCheck)

module.exports = router