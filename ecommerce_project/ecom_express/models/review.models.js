const mongoose = require('mongoose')

const ReviewSchema = new mongoose.Schema({

        user_id : {
            type : String,
            required : true 
        },
        firstname : {
            type : String,
            required : true 
        },
        book_id : {
            type : String,
            required : true
        },
        review : {
            type : String,
            required : true
        }

})

module.exports = mongoose.model('review', ReviewSchema)