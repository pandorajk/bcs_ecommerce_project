const mongoose = require('mongoose')

const BookSchema = new mongoose.Schema({

        title : {
            type : String,
            required : true
        },
        author : {
            type : String,
            required : true 
        },
        publishing : {
            type : String,
            required : true 
        },
        isbn_asin : {
            type : String,
            required : true, 
            unique : true, 
        },
        price : {
            type : Number,
            required : true 
        }, 
        genre_id : {          
            type : String,
            required : true
        },
        genre_id2 : {          
            type : String
        },
        bestseller : {
            type : Boolean,
            required : true
        },
        stock : {
            type : Number,
            required : true
        }, 
        image : {
            type : String,
            required : true
        }, 
        blurb : {
            type : String,
            required : true
        },        
        botm : {
            type : Boolean,
            required : true,
            default : false
        }
})

module.exports = mongoose.model('book', BookSchema)