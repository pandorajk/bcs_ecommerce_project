const mongoose = require('mongoose')

const GenreSchema = new mongoose.Schema({

        genre : {
            type : String,
            required : true, 
            unique : true, 
        }
})

module.exports = mongoose.model('genre', GenreSchema)