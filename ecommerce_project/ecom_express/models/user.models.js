const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({

        firstname :   { type : String, required : true},
        lastname :    { type : String, required : true }, 
        email:        { type:String, unique:true, required:true },
        password :    { type : String, required : true }, 
        adminAccess : { type : Boolean, require: true, default : false}
})

module.exports = mongoose.model('user', UserSchema)