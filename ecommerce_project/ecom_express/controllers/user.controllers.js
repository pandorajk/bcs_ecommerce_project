const User       = require('../models/user.models'); 
const bcrypt     = require('bcrypt');
const jwt        = require('jsonwebtoken');
const config     = require('../config');
const saltRounds = 10;

const signup = async (req,res) => {
    const { firstname, lastname, email , password , password2 } = req.body;
    console.log(req.body)
	if( !email || !password || !password2) return res.json({ok:false,message:'All fields are required'});
    if(  password !== password2) return res.json({ok:false,message:'Passwords must match'});
    try{
    	const user = await User.findOne({ email })
    	if( user ) return res.json({ok:false,message:'Email already in use'});
    	const hash = await bcrypt.hash(password, saltRounds)
        console.log('hash =' , hash)
        const newUser = {
            firstname,
            lastname,
        	email,
        	password : hash
        }
        const create = await User.create(newUser)
        res.json({ok:true,message:'Successful Register'})
    }catch( error ){
        res.json({ok:false,error})
    }
}

const login = async (req,res) => {
    const { email , password} = req.body;
    console.log(req.body)
    if( !email || !password ) res.json({ok:false,message:'All fields are required'});
	try{
    	const user = await User.findOne({ email });
        if( !user ) return res.json({ok:false,message:'This email is not recognised. Please try another or signup'});
        const match = await bcrypt.compare(password, user.password);
        if(match) {
           const token = jwt.sign(user.toJSON(), config.secret ,{ expiresIn:100080 });
           res.json({ok:true,message:'Welcome back',token,email}) 
        } else return res.json({ok:false,message:'Invalid password'})
        
    }catch( error ){
    	 res.json({ok:false,error})
    }
}

const verify_token = (req,res) => {
       const { token } = req.body;
       const decoded   = jwt.verify(token, config.secret, (err,succ) => {
           console.log(succ)
             err ? res.json({ok:false,message:'something went wrong with verify_token'}) 
                 : res.json({ok:true,message:'server message to members area', adminAccess : succ.adminAccess, firstname : succ.firstname, email : succ.email, user_id : succ._id})
       });      
}

// => FIND USER/MEMBER WITH EMAIL
const findMember  = async (req, res) => {
    const { email } = req.body
    try {
        const response = await User.findOne({email})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => UPDATE ADMIN ACCESS
// working in postman? Y 
const update_admin = async (req, res) => {
    const { _id, adminAccess } = req.body
    try {
        const response = await User.update({_id}, {$set:{ adminAccess}})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

module.exports = { signup , login , verify_token, findMember, update_admin }