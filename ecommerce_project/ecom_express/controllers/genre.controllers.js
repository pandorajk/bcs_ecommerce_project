const Genre = require('../models/genre.models')

// => CREATE A NEW GENRE
// working in postman? Y
const create = async (req, res) => {
    const  genre  = req.body.genre
                            // NB req.body.*TEXT HERE* has to match UI
    try {
        const response = await Genre.create({genre})
                                  // must match body (key: value) from POSTMAN 
        res.send({response})
    }
    catch (error) {
        res.send(error)
    }
}

// => REMOVE A GENRE (BY NAME)
// working in postman? Y/N
const remove = async (req, res) => {
    const { genre } = req.body 
    console.log(genre)
    try {
        const response = await Genre.deleteOne({genre})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => UPDATE A GENRE'S NAME
// working in postman? Y/N
const update = async (req, res) => {
    const { genre, updateGenre } = req.body
    try {
        const response = await Genre.update({genre : genre}, {$set:{genre : updateGenre}})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => SHOW ALL GENRES
// working in postman? Y
const findAllGenres  = async (req, res) => {
    try {
        const response = await Genre.find({})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND GENRE BY GENRE
// working in postman? Y
const findGenreByGenreID = async (req, res) => {
    const { _id } = req.body
    // console.log(req.body)
    try {
        const response = await Genre.find({_id})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

module.exports = {
    create, 
    remove, 
    update,
    findAllGenres, 
    findGenreByGenreID
}