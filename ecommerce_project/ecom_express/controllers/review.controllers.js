const Review = require('../models/review.models')

const createReview = async (req, res) => { 
    const { user_id, firstname, book_id, review } = req.body  
    console.log(req.body)  
    try {
        const response = await Review.create({ user_id, firstname, book_id, review  })
        res.send({ok:true,message:'Review created'})
    }
    catch (error) {
        console.log(error)
        res.send(error)
    }
}

const findReviews  = async (req, res) => {
    try {
        const response = await Review.find({})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

const removeReview = async (req, res) => {
    const { _id } = req.body 
    try {
        const response = await Review.deleteOne({_id})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

module.exports = {
    createReview, 
    findReviews, 
    removeReview, 
}