const Book = require('../models/book.models')

// => CREATE A NEW BOOK
// working in postman? Y
const create = async (req, res) => { 
    const { title, author, publishing, isbn_asin, price, genre_id, genre_id2, bestseller, stock, image, blurb, botm } = req.body    
    
    console.log('=======================>',  req.body)
   
    if(!title || !author || !publishing || !isbn_asin || !price || !genre_id || !bestseller || !stock || !image || !blurb || !botm) {
        return res.send({ok:false,message:'All the fields are required'})
    }

    try {
        const response = await Book.create({ title, author, publishing, isbn_asin, price, genre_id, genre_id2, bestseller, stock, image, blurb, botm})
        res.send({ok:true,message:'Book created'})
    }
    catch (error) {
        console.log(error)
        res.send(error)
    }
}

// => REMOVE A BOOK
// working in postman? Y
const remove = async (req, res) => {
    const { book_id } = req.body  
    // console.log(book_id)
    try {
        const response = await Book.deleteOne({_id: book_id})
        res.send({response})
    } 
    catch (error) {
        // console.log(error)
        res.send(error)
    }
}

// => UPDATE A BOOK'S TITLE
// working in postman? N
const update = async (req, res) => {
    const { book_id, title, author, publishing, isbn_asin, price, genre_id, genre_id2, bestseller, stock, image, blurb,  botm } = req.body
    try {
        const response = await Book.update({_id : book_id}, {$set:{ title, author, publishing, isbn_asin, price, genre_id, genre_id2, bestseller, stock, image, blurb,  botm }})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

const updateStock = async (req, res) => {
    const { _id, newStock } = req.body
    try {
        const response = await Book.update({_id}, {$set:{ stock : newStock }})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => SHOW ALL BOOKS AND THEIR DETAILS
// working in postman? Y
const findAllBooks  = async (req, res) => {
    try {
        const response = await Book.find({})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => SHOW ALL BOOKS FROM ONE GENRE
// working in postman? Y
const findBooksByGenre  = async (req, res) => {
    const { genre_id } = req.body
    // console.log(req.body)
    try {
        // const response = await Book.find({genre_id2 : genre_id}) =====>  this only found one genre... below is the solution to find books with genre matching genre_id OR genre_id2
        const response = await Book.find().or([{genre_id : genre_id},{genre_id2 : genre_id}])
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND BOOK WITH ITS TITLE
// working in postman? Y
const findBookByTitle  = async (req, res) => {
    const { title } = req.body
    // console.log(req.body)
    try {
        const response = await Book.findOne({title})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND BOOK WITH ITS ID
// working in postman? Y/N
const findBookByID  = async (req, res) => {
    const { book_id } = req.body
    // console.log(req.body)
    try {
        const response = await Book.findOne({_id : book_id})
        console.log(response)
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND BOOKS WITH THIER IDS  **** MULTIPLE BOOKSS******
// working in postman? Y/N
const findBooksByID  = async (req, res) => {
    const { book_id } = req.body
    // console.log(req.body)
    try {
        const response = await Book.find({_id : book_id})
        console.log(response)
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND BOOK OF THE MONTH
// working in postman? Y/N
const findBOTM  = async (req, res) => {
    try {
        const response = await Book.findOne({botm : true})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND BESTSELLERS
const findBestsellers  = async (req, res) => {
    try {
        const response = await Book.find({bestseller : true})
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

// => FIND BESTSELLERS
const findBooksAny  = async (req, res) => {
    const { any } = req.body
    try {
        const response = await Book.find().or([{title : any}, {author : any},  {isbn_asin : any}])
        res.send({response})
    } 
    catch (error) {
        res.send(error)
    }
}

module.exports = {
    create,
    remove,
    update, 
    updateStock,
    findAllBooks,
    findBooksByGenre, 
    findBookByTitle,
    findBookByID, 
    findBooksByID,
    findBOTM, 
    findBestsellers, 
    findBooksAny
}