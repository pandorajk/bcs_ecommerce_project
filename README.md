# ECOMMERCE PROJECT

# Stack:
Full stack e-commerce website built with MERN stack: React.js (^16.9.0) on the client side, and served-side built with Node.js, Express (^4.17.1), RESTful APIs and database in MongoDB (with Mongoose).


# Functionality: 
Online 'Latin American Bookstore' that allows users to browse products, add products to the basket and make purchases (using Stripe external API), members can register and sign-in with an authenticated login and review books once logged in. There is also an admin portal (with unqiue admin login details) with full CRUD operations available to create and edit products. 


# Deployment:
See website at IP address: 
        67.207.81.123 
Website deployed with Digital Ocean. 


# Homepage screenshot: 
https://www.dropbox.com/s/58023ieu5f8zxxv/ecommerce-project.png?dl=0


# Set up and Installation: 
1. git clone https://gitlab.com/Pandorajk/bcs_ecommerce_project.git
2. Download and install below packages with npm install. Globally install: $ npm install -g nodemon
3. Client start: cd ecom_react && npm start
4. Server start: cd ecom_express && nodemon index.js
5. Have database running with MongDB (See: 'Install MongoDB Community Edition' documentation: https://docs.mongodb.com/manual/installation/)
6. When "purchasing" products, use card number 4242 4242 42424 4242, CVC 123 and Expiry Date as any date in the future.
7. The following 4 part tutorial on installing and using the MERN stack is very helpful (especially part 1 and 2): https://codingthesmartway.com/the-mern-stack-tutorial-building-a-react-crud-application-from-start-to-finish-part-1/

> React/client packages and dependencies: 
    - axios : "^0.19.0",
    - cors: "^2.8.5",
    - react : "^16.9.0"
    - react-dom: "^16.9.0",
    - react-router-dom: "^5.0.1",
    - react-scripts: "3.1.2",
    - react-stripe-elements: "^5.0.1"

> Express/server packages and dependencies:
    - bcrypt: "^3.0.6",
    - body-parser: "^1.19.0",
    - cors: "^2.8.5",
    - express: "^4.17.1", 
    - jsonwebtoken: "^8.5.1",
    - mongoose: "^5.7.1",
    - stripe: "^7.9.1"
    

# Commit history: 
1. Initial commit - express/server-side built with book schema with Mongoose.(27/09/2019)
2. UI added with React. Minimalist layout. Adjustments made in express. 
3. Authentication (server and client) added, login as user or login as admin options created with different access options
4. Basket and Payment added - basket quantity changer and remover complete - payment function working but stock needs to be checked and updated.
5. Further updates to basket and payment.
6. Build folder added for first deployment on Digital Ocean
7. Payment now updates Stock, App now has full functionality and meets breif - although many parts now need refactoring and revewing
8. Review book feature created - with new server-side controllers/routes/models and MongoDB collections
9. Debugged Review feature - now it's working
10. Multiple bug fixes, homepage search and signup inputs now active.  Users and Admin can now delete unwanted reviews. Other issues/comments are written in notes in the code. 

# Contact:
Pandora Jane Knocker - pjknocker@yahoo.com